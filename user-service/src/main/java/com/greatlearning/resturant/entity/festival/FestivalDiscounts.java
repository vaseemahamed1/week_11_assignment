package com.greatlearning.resturant.entity.festival;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class FestivalDiscounts {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @NotNull
    @Column(unique = true)
    private String festivalName;

    @NotNull
    @Column(unique = true)
    private String discountCoupon;

    public FestivalDiscounts() {
    }

    public FestivalDiscounts(@NotNull String festivalName, @NotNull String discountCoupon) {
        this.festivalName = festivalName;
        this.discountCoupon = discountCoupon;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFestivalName() {
        return festivalName;
    }

    public void setFestivalName(String festivalName) {
        this.festivalName = festivalName;
    }

    public String getDiscountCoupon() {
        return discountCoupon;
    }

    public void setDiscountCoupon(String discountCoupon) {
        this.discountCoupon = discountCoupon;
    }

}
