package com.greatlearning.resturant.entity;

public class UserDto {
    private String username;
    private String password;
    private String city;

    public String getUsername() {
        return username;
    }

    public UserDto(String username, String password, String city) {
		super();
		this.username = username;
		this.password = password;
		this.city = city;
	}

	public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "UserDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                '}';
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }
}
