package com.greatlearning.resturant.entity.sales;

public class SalesResponseDto {

    private String criteria;
    private Number sales;

    public SalesResponseDto(String name, Number totalSale) {
        this.criteria = name;
        this.sales = totalSale;
    }

    public String getCriteria() {
        return criteria;
    }

    public void setCriteria(String criteria) {
        this.criteria = criteria;
    }

    public Number getSales() {
        return sales;
    }

    public void setSales(Number sales) {
        this.sales = sales;
    }
}
