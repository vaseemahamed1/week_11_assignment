package com.greatlearning.resturant.service;

import com.greatlearning.resturant.entity.BillDetailsDtoList;
import com.greatlearning.resturant.entity.InventoryDetails;
import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.entity.sales.SalesResponseList;
import org.springframework.security.core.Authentication;


import java.sql.Date;
import java.util.List;
import java.util.Optional;

public interface OrdersService {

    BillDetailsDtoList generateTodayBills();

    TotalSalesDto getTotalSalesForThisMonth();

    void saveOrder(Authentication authentication, Integer selectedItem, Optional<InventoryDetails> item);

    List<Orders> getByUsername(Authentication authentication);

    List<Orders> getAllOrdersUsingView(String userName);

    List<Orders> getOrdersByDateUsingView(Date date, String name);

    List<Orders> getOrdersByPriceUsingView(Integer price, String name);

    SalesResponseList getSalesInDifferentCities();

    SalesResponseList getMaxSalesInTheMonth();

    SalesResponseList getMonthlySalesInLastYear();
}
