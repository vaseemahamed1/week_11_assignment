package com.greatlearning.resturant.config;

import com.greatlearning.resturant.entity.AuditLog;
import com.greatlearning.resturant.repository.AuditLogRepository;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.core.context.SecurityContextHolder;

import java.sql.Date;

@Configuration
@Aspect
public class AspectConfig {

    @Autowired
    AuditLogRepository auditLogRepository;

    //add log to audit table when user generate the bill
    @After("execution(public * com.greatlearning.resturant.controller.SalesController.getTotalBill(..))")
    public void logUserBillGeneration() {
        auditLogRepository.save(new AuditLog(new Date(System.currentTimeMillis()), SecurityContextHolder.getContext()
                .getAuthentication().getName() + " generated bill for his orders"));
    }

    //add log to audit table when admin generate the bill
    @After("execution(public * com.greatlearning.resturant.serviceImpl.OrdersServiceImpl.generateTodayBills(..))")
    public void logGenerateTodayBills() {
        auditLogRepository.save(new AuditLog(new Date(System.currentTimeMillis()), "Generated bill for today "));
    }

    //add log to audit table when admin generate the bill for month
    @After("execution(public * com.greatlearning.resturant.serviceImpl.OrdersServiceImpl.getTotalSalesForThisMonth(..))")
    public void logFetTotalSalesForThisMonth() {
        auditLogRepository.save(new AuditLog(new Date(System.currentTimeMillis()), "Generated bill for current month "));
    }
}
