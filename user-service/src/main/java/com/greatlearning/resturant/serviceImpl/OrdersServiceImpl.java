package com.greatlearning.resturant.serviceImpl;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.entity.sales.SalesResponseDto;
import com.greatlearning.resturant.entity.sales.SalesResponseList;
import com.greatlearning.resturant.repository.OrderRepository;
import com.greatlearning.resturant.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class OrdersServiceImpl implements OrdersService {

    @Autowired
    OrderRepository orderRepository;

    @Override
    public BillDetailsDtoList generateTodayBills() {
        BillDetailsDtoList response = new BillDetailsDtoList();
        List<BillDetailsDto> bills = new ArrayList<>();
        // find all order based on date and generate the bill for current date
        orderRepository.findAllByOrderDate(new Date(System.currentTimeMillis()))
                .stream()
                .collect(Collectors.groupingBy(it -> it.getUsername()))
                .forEach((s, orders) -> {
                    List<String> items = new ArrayList<>();
                    Integer totalAmt = 0;
                    for (Orders order : orders) {
                        items.add(order.getItemName());
                        totalAmt = totalAmt + order.getPrice();
                    }
                    // bill object being added
                    bills.add(new BillDetailsDto(orders.get(0).getUsername(), items.stream().distinct().collect(Collectors.toList()), totalAmt));
                });
        response.setBillDetailsDtoList(bills);
        return response;
    }

    @Override
    public TotalSalesDto getTotalSalesForThisMonth() {
        // get all orders based on current month and generate total sales
        Integer totalSale = 0;
        for (Orders it : orderRepository.findAllByOrderDateMonth(LocalDate.now()
                .getMonthValue())) {
            totalSale = totalSale + it.getPrice();
        }
        return new TotalSalesDto(LocalDate.now().getMonth().name(), totalSale);
    }

    @Override
    public void saveOrder(Authentication authentication, Integer selectedItem, Optional<InventoryDetails> item) {
        // save the order go order table
        orderRepository.save(
                new Orders(authentication.getName(), selectedItem,
                        new Date(System.currentTimeMillis()),
                        item.get().getItemName(), item.get().getPrice()));
    }

    @Override
    public List<Orders> getByUsername(Authentication authentication) {
        return orderRepository.findByUsername(authentication.getName());
    }

    @Override
    public List<Orders> getAllOrdersUsingView(String userName) {
        return orderRepository.findAllOrdersUsingView(userName);
    }

    @Override
    public List<Orders> getOrdersByDateUsingView(Date date, String name) {
        return orderRepository.findOrdersByDateUsingView(date, name);
    }

    @Override
    public List<Orders> getOrdersByPriceUsingView(Integer price, String name) {
        return orderRepository.findOrdersByPriceUsingView(price, name);
    }

    @Override
    public SalesResponseList getSalesInDifferentCities() {
        SalesResponseList salesList = new SalesResponseList();
        List<SalesResponseDto> dtoList = new ArrayList<>();
        orderRepository.findSalesPerCity().forEach((s, integer) -> dtoList.add(new SalesResponseDto(s, integer)));
        salesList.setSalesResponseDtoList(dtoList);
        return salesList;
    }

    @Override
    public SalesResponseList getMaxSalesInTheMonth() {
        SalesResponseList salesList = new SalesResponseList();
        List<SalesResponseDto> dtoList = new ArrayList<>();
        orderRepository.findMaxSalesInTheMonth().forEach((s, integer) -> dtoList.add(new SalesResponseDto(s, integer)));
        salesList.setSalesResponseDtoList(dtoList);
        return salesList;
    }

    @Override
    public SalesResponseList getMonthlySalesInLastYear() {
        SalesResponseList salesList = new SalesResponseList();
        List<SalesResponseDto> dtoList = new ArrayList<>();
        orderRepository.findMonthlySalesInLastYear().forEach((s, integer) -> dtoList.add(new SalesResponseDto(s, integer)));
        salesList.setSalesResponseDtoList(dtoList);
        return salesList;
    }

}
