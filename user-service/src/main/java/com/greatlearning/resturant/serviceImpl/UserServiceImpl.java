package com.greatlearning.resturant.serviceImpl;

import com.greatlearning.resturant.entity.UserDto;
import com.greatlearning.resturant.entity.Users;
import com.greatlearning.resturant.repository.UserRepository;
import com.greatlearning.resturant.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @Override
    public Users createUser(UserDto userDto) {
        return userRepository.save(buildUserObject(userDto));
    }

    @Override
    public Users getUser(String userName) {
        return userRepository.getByUsername(userName);
    }

    @Override
    public void deleteUser(String userName) {
        userRepository.deleteByUsername(userName);
    }

    @Override
    public Users updateUser(String userName, UserDto userDto) {
        //update user details
        Users existingUser = userRepository.getByUsername(userName);
        existingUser.setUsername(userDto.getUsername());
        existingUser.setPassword(passwordEncoder.encode(userDto.getPassword()));
        return userRepository.save(existingUser);
    }

    private Users buildUserObject(UserDto userDto) {
        return new Users(userDto.getUsername(), passwordEncoder.encode(userDto.getPassword()),
                "ROLE_USER", "TRUE", userDto.getCity());
    }

}
