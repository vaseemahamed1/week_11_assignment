package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.entity.festival.FestivalDiscounts;
import com.greatlearning.resturant.repository.InventoryDetailsRepository;
import com.greatlearning.resturant.service.OrdersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/orders")
public class OrdersController {

    @Autowired
    private RestTemplate template;

    @Autowired
    InventoryDetailsRepository inventoryDetailsRepository;

    @Autowired
    OrdersService ordersService;

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping("/viewMenuItems")
    public InventoryDetailsList getMenuItems() {
        InventoryDetailsList response = new InventoryDetailsList();
        response.setInventoryDetailsList(inventoryDetailsRepository.findAll());
        return response;
    }

    @PostMapping("/selectItems")
    public String selectMenuItems(@RequestParam List<Integer> menuItemsIds,
                                  @RequestParam(required = false) String discountCoupon) {
        // get user name from spring security
        Authentication authentication = getAuthentication();

        // check coupon is valid or not
        boolean validCoupon = checkCouponValidity(discountCoupon);

        // map menu items
        menuItemsIds.stream()
                .forEach(selectedItem -> {
                    Optional<InventoryDetails> item = inventoryDetailsRepository.findById(selectedItem);
                    if (item.isPresent()) {
                        if (validCoupon) {
                            item.get().setPrice(item.get().getPrice() / 2);
                        }
                        ordersService.saveOrder(authentication, selectedItem, item);
                    }
                });
        return "order added";
    }

    private boolean checkCouponValidity(String discountCoupon) {
        if (discountCoupon != null) {
            String url = "http://FESTIVALSALE-SERVICE/festivals/validateDiscountCoupon/" + discountCoupon;
            FestivalDiscounts response = template.getForObject(url, FestivalDiscounts.class);
            if (response != null) {
                return true;
            } else throw new RuntimeException("invalid coupon");
        }
        return false;
    }

    @GetMapping("/viewAllOrders")
    public List<Orders> viewAllOrders() {
        return ordersService.getAllOrdersUsingView(getAuthentication().getName());
    }

    @GetMapping("/viewOrdersByDate")
    public List<Orders> viewOrdersByDate(Date date) {
        return ordersService.getOrdersByDateUsingView(date, getAuthentication().getName());
    }

    @GetMapping("/viewOrdersByPrice")
    public List<Orders> viewOrdersByPrice(Integer price) {
        return ordersService.getOrdersByPriceUsingView(price, getAuthentication().getName());
    }
}
