package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.repository.InventoryDetailsRepository;
import com.greatlearning.resturant.service.OrdersService;
import com.greatlearning.resturant.service.UserService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@RestController
public class UserController {

    @Autowired
    private RestTemplate template;

    @Autowired
    UserService userService;

    @GetMapping("/welcome")
    public String home() {
        return ("<h1> Welcome User </h1>");
    }

    @PostMapping("/register")
    public Users register(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }

    @ApiOperation(value = "viewUser", hidden = true)
    @GetMapping("/viewUser/{userName}")
    public Users getUser(@PathVariable String userName) {
        return userService.getUser(userName);
    }

    @ApiOperation(value = "createUser", hidden = true)
    @PostMapping("/createUser")
    public Users createUser(@RequestBody UserDto userDto) {
        return userService.createUser(userDto);
    }

    @ApiOperation(value = "updateUser", hidden = true)
    @PutMapping("/updateUser/{userName}")
    public Users updateUser(@PathVariable String userName, @RequestBody UserDto userDto) {
        return userService.updateUser(userName, userDto);
    }

    @ApiOperation(value = "deleteUser", hidden = true)
    @DeleteMapping("/deleteUser/{userName}")
    @Transactional
    public String deleteUser(@PathVariable String userName) {
        userService.deleteUser(userName);
        return "user deleted";
    }
}
