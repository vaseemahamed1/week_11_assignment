package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.BillDetailsDto;
import com.greatlearning.resturant.entity.BillDetailsDtoList;
import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.TotalSalesDto;
import com.greatlearning.resturant.entity.festival.FestivalDiscountsList;
import com.greatlearning.resturant.entity.sales.SalesResponseList;
import com.greatlearning.resturant.service.OrdersService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/sales")
public class SalesController {

    @Autowired
    private RestTemplate template;

    @Autowired
    OrdersService ordersService;

    private Authentication getAuthentication() {
        return SecurityContextHolder.getContext().getAuthentication();
    }

    @GetMapping("/getBill")
    public BillDetailsDto getTotalBill() {
        Authentication authentication = getAuthentication();
        List<String> items = new ArrayList<>();
        Integer totalAmt = 0;
        // create bill list
        for (Orders it : ordersService.getByUsername(authentication)) {
            items.add(it.getItemName());
            totalAmt = totalAmt + it.getPrice();
        }
        // bill builder object
        return new BillDetailsDto(authentication.getName(), items.stream().distinct().collect(Collectors.toList()), totalAmt);
    }

    @GetMapping("/festivals/getAllDiscountCoupons")
    public FestivalDiscountsList getAllDiscountCoupons() {
        String url = "http://FESTIVALSALE-SERVICE/festivals/getAllDiscountCoupons";
        return template.getForObject(url, FestivalDiscountsList.class);
    }

    @ApiOperation(value = "This method is used to generate bill for today", hidden = true)
    @GetMapping("/generateTodayBills")
    public BillDetailsDtoList generateTodayBills() {
        return ordersService.generateTodayBills();
    }

    @ApiOperation(value = "This method is used to generate bill for current month", hidden = true)
    @GetMapping("/getTotalSalesForCurrentMonth")
    public TotalSalesDto getTotalSalesForMonth() {
        return ordersService.getTotalSalesForThisMonth();
    }

    @ApiOperation(value = "This method is used get sales in diff cities", hidden = true)
    @GetMapping("/getSalesInDifferentCities")
    public SalesResponseList getSalesInDifferentCities() {
        return ordersService.getSalesInDifferentCities();
    }

    @ApiOperation(value = "This method is used to get max sales in the month", hidden = true)
    @GetMapping("/getMaxSalesInTheMonth")
    public SalesResponseList getMaxSalesInTheMonth() {
        return ordersService.getMaxSalesInTheMonth();
    }

    @ApiOperation(value = "This method is used to get monthly sales in last year", hidden = true)
    @GetMapping("/getMonthlySalesInLastYear")
    public SalesResponseList getMonthlySalesInLastYear() {
        return ordersService.getMonthlySalesInLastYear();
    }

}
