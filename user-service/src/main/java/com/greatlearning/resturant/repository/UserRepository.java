package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<Users, Integer> {

    Users getByUsername(String userName);

    void deleteByUsername(String userName);

}
