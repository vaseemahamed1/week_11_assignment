package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.Orders;
import com.greatlearning.resturant.entity.sales.SalesResponseDto;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;
import java.util.Map;

public interface OrderRepository extends JpaRepository<Orders, Integer> {
    List<Orders> findByUsername(String name);

    List<Orders> findAllByOrderDate(Date date);

    @Query(value = "select * from ORDERS where month(order_date)= ?1", nativeQuery = true)
    List<Orders> findAllByOrderDateMonth(Integer month);

    @Query(value = "select * from ORDERS_VIEW where USERNAME = ?1", nativeQuery = true)
    List<Orders> findAllOrdersUsingView(String userName);

    @Query(value = "SELECT * FROM ORDERS_VIEW where ORDER_DATE = ?1 AND USERNAME = ?2", nativeQuery = true)
    List<Orders> findOrdersByDateUsingView(Date date, String userName);

    @Query(value = "SELECT * FROM ORDERS_VIEW where PRICE = ?1 AND USERNAME = ?2", nativeQuery = true)
    List<Orders> findOrdersByPriceUsingView(Integer price, String userName);

    @Query(value = "SELECT * FROM SALES_PER_CITY", nativeQuery = true)
    Map<String, Number> findSalesPerCity();

    @Query(value = "SELECT * FROM MAX_SALES_IN_MONTH", nativeQuery = true)
    Map<String, Number>  findMaxSalesInTheMonth();

    @Query(value = "SELECT * FROM MONTHLY_SALES_IN_LAST_YEAR", nativeQuery = true)
    Map<String, Number>  findMonthlySalesInLastYear();
}
