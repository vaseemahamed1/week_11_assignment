package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.FestivalDiscounts;
import org.springframework.data.jpa.repository.JpaRepository;

import javax.validation.constraints.NotNull;

public interface FestivalSaleRepository extends JpaRepository<FestivalDiscounts, Integer> {

    FestivalDiscounts findByDiscountCoupon(@NotNull String discountCoupon);
}
