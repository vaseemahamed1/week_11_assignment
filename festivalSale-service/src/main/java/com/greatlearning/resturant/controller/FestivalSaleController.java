package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.repository.FestivalSaleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.List;

@RestController
@RequestMapping("/festivals")
public class FestivalSaleController {

    @Autowired
    private FestivalSaleRepository festivalSaleRepository;


    @GetMapping("/getAllDiscountCoupons")
    public FestivalDiscountsList getAllCoupons() {
        FestivalDiscountsList response = new FestivalDiscountsList();
        response.setFestivalDiscountsList(festivalSaleRepository.findAll());
        return response;
    }

    @GetMapping("/validateDiscountCoupon/{couponCode}")
    public FestivalDiscounts validateDiscountCoupon(@PathVariable String couponCode) {
        try {
            return festivalSaleRepository.findByDiscountCoupon(couponCode);
        } catch (Exception exception) {
            throw new RuntimeException("invalid coupon");
        }
    }
}
