package com.greatlearning.resturant.repository;

import com.greatlearning.resturant.entity.Feedback;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.sql.Date;
import java.util.List;

public interface FeedbackRepository extends JpaRepository<Feedback, Integer> {
}
