package com.greatlearning.resturant.controller;

import com.greatlearning.resturant.entity.*;
import com.greatlearning.resturant.repository.FeedbackRepository;
import com.greatlearning.resturant.repository.SeatBookingRepository;

import io.swagger.models.auth.In;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import java.sql.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
public class OfflineUserController {

    @Autowired
    private RestTemplate template;

    @Autowired
    private SeatBookingRepository seatBookingRepository;

    @Autowired
    private FeedbackRepository feedbackRepository;

    @PostMapping("/seatBooking")
    public SeatBooking seatBooking(@RequestParam String userName) {
        return seatBookingRepository
                .save(new SeatBooking(userName, true, new Date(System.currentTimeMillis())));
    }

    @PostMapping("/feedback")
    public Feedback feedback(@RequestParam Integer rating, @RequestParam String comment) {
        return feedbackRepository
                .save(new Feedback(rating, comment, new Date(System.currentTimeMillis())));
    }

    @GetMapping("/viewMenu")
    public InventoryDetailsList getMenuItems() {
        String url = "http://USER-SERVICE/orders/viewMenu";
        return template.getForObject(url, InventoryDetailsList.class);
    }

    @PostMapping("/selectItems")
    public String createUser(@RequestParam List<Integer> menuItemsIds,
                             @RequestParam(required = false) String discountCoupon) {
        String url = "http://USER-SERVICE/orders/selectItems?";
        if (discountCoupon != null) {
            url = url + "discountCoupon=" + discountCoupon;
        }
        for (Integer itemNo : menuItemsIds) {
            url = url + "menuItemsIds=" + itemNo + "&";
        }
        return template.postForObject(url, null, String.class);
    }

    @GetMapping("/getBill")
    public String getBill() {
        String url = "http://USER-SERVICE/orders/getBill";
        return template.getForObject(url, String.class);
    }

//    @PostMapping("/billPayment")
//    public String billPayment(@RequestParam String paymentType) {
//        String url = "http://USER-SERVICE/selectItems/";
//        return template.postForObject(url, null, String.class);
//    }

}
