package com.greatlearning.resturant.entity;

public class TotalSalesDto {
    private String currentMonth;
    private Integer totalSale;

    public TotalSalesDto(String currentMonth, Integer totalSale) {
        this.currentMonth = currentMonth;
        this.totalSale = totalSale;
    }

    public TotalSalesDto() {
    }

    public String getCurrentMonth() {
        return currentMonth;
    }

    public void setCurrentMonth(String currentMonth) {
        this.currentMonth = currentMonth;
    }

    public Integer getTotalSale() {
        return totalSale;
    }

    public void setTotalSale(Integer totalSale) {
        this.totalSale = totalSale;
    }

    @Override
    public String toString() {
        return "TotalSalesDto{" +
                "currentMonth='" + currentMonth + '\'' +
                ", totalSale=" + totalSale +
                '}';
    }
}
