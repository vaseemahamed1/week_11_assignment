package com.greatlearning.resturant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@SpringBootApplication
@EnableEurekaServer
public class AggregatorMicroserviceApp {

	public static void main(String[] args) {
		SpringApplication.run(AggregatorMicroserviceApp.class, args);
	}
}
